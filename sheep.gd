extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const STATE_IDLE = "idle"
const STATE_FLOCKING = "flock"
const STATE_ROAM = "roam"

var destination = Vector2()
var state = STATE_IDLE
var blocked_move = Vector2(0, 0)
var turd_count = 0
var strength = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)

func _process(delta):
	for i in get_node("Area2D").get_overlapping_areas():
		var n = i.get_parent();
		if n.has_method('is_eatable'):
			n.eat(self)

	if state == STATE_IDLE:
		if randf() < 0.05:
			var d = pick_random_destination()
			#print("I want wander to ", d)
			destination = d
			state = STATE_ROAM
	if state == STATE_ROAM:
		if destination.distance_to(get_global_pos()) < 50:
			#print("I have arrived")
			state = STATE_IDLE
	if state == STATE_ROAM or state == STATE_FLOCKING:
		var dir = (destination - get_global_pos()).normalized()
		var motion = dir * rand_range(20, 100) * delta
		var blocked = move(motion)
		if blocked - blocked_move != Vector2(0, 0):
			#print(blocked - blocked_move)
			if state == STATE_ROAM:
				state = STATE_IDLE
			#print("I think I might be stuck")
		blocked_move = blocked
		
	if turd_count > 0:
		poop()

func flock(d, enable = true):
	if not enable:
		state = STATE_ROAM
		destination = pick_random_destination()
		return
	destination = d
	state = STATE_FLOCKING
	
func pick_random_destination():
	var v = get_viewport_rect()
	return Vector2(rand_range(v.pos.x, v.end.x), rand_range(v.pos.y, v.end.y))

func poop():
	turd_count -= 1
	var t = preload("res://poop.tscn")
	var n = t.instance()
	n.set_global_pos(get_global_pos())
	get_node('/root/game/Level').add_child(n)